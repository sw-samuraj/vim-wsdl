" WSDL and XSD set up for Tagbar plugin
" (https://github.com/majutsushi/tagbar).

if !exists(':Tagbar')
    finish
endif

let g:tagbar_type_xml = {
    \ 'ctagstype' : 'WSDL',
    \ 'kinds'     : [
        \ 'n:namespaces',
        \ 'm:messages',
        \ 'p:portType',
        \ 'o:operations',
        \ 'b:bindings',
        \ 's:service'
    \ ],
    \ 'deffile'   : expand('<sfile>:p:h:h') . '/ctags/wsdl.ctags'
\ }
let g:tagbar_type_xsd = {
    \ 'ctagstype' : 'XSD',
    \ 'kinds' : [
        \ 'e:elements',
        \ 'c:complexTypes',
        \ 's:simpleTypes'
    \ ],
    \ 'deffile'   : expand('<sfile>:p:h:h') . '/ctags/xsd.ctags'
\ }
