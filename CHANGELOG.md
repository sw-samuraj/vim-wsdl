# vim-wsdl Plugin Changelog #

## 0.1 ##

Initial version.

### New Features ###

* Add basic *Ctags* support for *Tagbar* for `WSDL` and `XSD` files.
